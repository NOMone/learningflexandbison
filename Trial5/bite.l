%{
#include <stdint.h>

#include "bite.tab.h"
#define YY_DECL extern "C" int yylex()

namespace Bite {
    extern int32_t lineNumber;
}

%}
%option noyywrap
%%
[ \t] ;
\n              { Bite::lineNumber++; }
[1-9][0-9]*    	{ yylval.intValue = atoi(yytext); return INT; }
[a-zA-Z0-9]+   	{
                    // we have to copy because we can't rely on yytext not changing underneath us:
                    yylval.identifier = strdup(yytext); // TODO: solve the memory leak...
                    return ID;
                }
.               { return *yytext; }
%%
