%{

#include "bite.h"

#include <cstdio>
#include <iostream>
#include <stdint.h>
#include <string>
#include <vector>

using namespace Bite;
using namespace std;

// stuff from flex that bison needs to know about:
extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;
 
void yyerror(const char *s);

%}

// Bison fundamentally works by asking flex to get the next token, which it
// returns as an object of type "yystype".  But tokens could be of any
// arbitrary data type!  So we deal with that in Bison by defining a C union
// holding each of the types of tokens that Flex could return, and have Bison
// use that union instead of "int" for the definition of "yystype":
%union {
	int32_t intValue;
	char *identifier;
}

// define the constant-string tokens:
//%token SNAZZLE TYPE
//%token END

// define the "terminal symbol" token types I'm going to use (in CAPS
// by convention), and associate each with a field of the union:
%token <intValue> INT
%token <identifier> ID

%%
CompilationUnit:
   Statements
   ;

Statements:
   Statement
   | Statements Statement
   ;

Statement:
   Declaration
   | Assignment   
   ;

Declaration:
   ID DeclaredList ';'  	{ performDeclarationStatement($1); }
   ;

DeclaredList:
   ID						{ createDeclarationStatement($1); }
   | DeclaredList ',' ID 	{ addVariableToDeclarationStatement($3); }
   ;

Assignment:
   ID '=' Expression ';'
   ;

Expression:
	Precedence1
	;

Precedence1:
	Precedence1 '+' Precedence2    { generateAdd(); }
	| Precedence1 '-' Precedence2	 { generateSub(); }
	| Precedence2	
	;

Precedence2:
	Precedence2 '*' Precedence3    { generateMul(); }
	| Precedence2 '/' Precedence3  { generateDiv(); }
	| Precedence3
	;

Precedence3:
	INT   { generateImmediateInt($1); }
   | ID  { generateVariable($1); } // TODO: delete string?
	;

%%

int main(int, char**) {

    // Initialize bite compiler,
    Bite::initialize();

	// open a file handle to a particular file:
	FILE *myfile = fopen("InputFile.txt", "r");
	// make sure it's valid:
	if (!myfile) {
		cout << "Couldn't open InputFile.txt" << endl;
		return -1;
	}
	// set flex to read from it instead of defaulting to STDIN:
	yyin = myfile;

	// parse through the input until there is no more:
	do {
		yyparse();
	} while (!feof(yyin));
	
	vector<string> &errors = Bite::getErrors();
	for (int32_t i=0; i<errors.size(); i++) {
	   cout << errors[i] << '.' << endl;
	}
}

void yyerror(const char *s) {
	cout << "EEK, parse error!  Message: " << s << endl;
	// might as well halt now:
	exit(-1);
}
