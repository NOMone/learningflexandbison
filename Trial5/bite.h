#pragma once

#include <stdint.h>
#include <string>
#include <vector>

namespace Bite {

    std::vector<std::string> &getErrors();
    void initialize();

    void createDeclarationStatement(const char *firstVariableId);
    void addVariableToDeclarationStatement(const char *variableId);
    void performDeclarationStatement(const char *variablesType);

    void generateImmediateInt(int32_t value);
    void generateVariable(const char *id);
    void generateAdd();
    void generateSub();
    void generateMul();
    void generateDiv();
}
