#include "bite.h"

#include <iostream>
#include <stack>
#include <string>

using namespace Bite;
using namespace std;

namespace Bite {
    int32_t lineNumber=1;
}

vector<string> errors;
void addError(string error) {
    errors.push_back(error);
}

vector<string> &Bite::getErrors() {
    return errors;
}

class Statement {
public:
    virtual ~Statement() {}
};

class DataType {
public:
    string name;
};

class Variable {
public:
    string name;
    int32_t typeIndex;
};

class Scope {
public:
    stack<Statement *> statements;
    vector<DataType> dataTypes;

    int32_t findDataType(std::string name) {
        for (int32_t i=0; i<dataTypes.size(); i++) {
            if (dataTypes[i].name == name) return i;
        }
        return -1;
    }
};

Scope currentScope;

class DeclarationStatement : public Statement {
    vector<std::string> variableNames;
public:
    void addVariable(std::string id) { variableNames.push_back(id); }
    void perform(std::string variablesType) {
        if (currentScope.findDataType(variablesType) == -1) {
            addError("Line " + std::to_string(lineNumber) + ": Type '" + variablesType + "' not defined");
        }

        // TODO: Add the variables to the scope ...
        // Generate stack offsetting...
    }
};

void Bite::initialize() {

    // Add basic data-types,
    DataType dataType;
    dataType.name = "int";
    //currentScope.dataTypes.push_back(dataType);
}

void Bite::createDeclarationStatement(const char *firstVariableId) {
    DeclarationStatement *newStatement = new DeclarationStatement();
    newStatement->addVariable(firstVariableId);
    currentScope.statements.push(newStatement);
}

void Bite::addVariableToDeclarationStatement(const char *variableId) {
    ((DeclarationStatement *) currentScope.statements.top())->addVariable(variableId);
}

void Bite::performDeclarationStatement(const char *variablesType) {
    ((DeclarationStatement *) currentScope.statements.top())->perform(variablesType);
}

void Bite::generateImmediateInt(int32_t value) {
    cout << "Push " << value << endl;
}

void Bite::generateVariable(const char *id) {
}
 
void Bite::generateAdd() {
    cout << "Pop r2" << endl;
    cout << "Pop r1" << endl;
    cout << "Add r1, r2" << endl;
    cout << "Push r1" << endl;
}

void Bite::generateSub() {
    cout << "Pop r2" << endl;
    cout << "Pop r1" << endl;
    cout << "Sub r1, r2" << endl;
    cout << "Push r1" << endl;
}

void Bite::generateMul() {
    cout << "Pop r2" << endl;
    cout << "Pop r1" << endl;
    cout << "Mul r1, r2" << endl;
    cout << "Push r1" << endl;
}

void Bite::generateDiv() {
    cout << "Pop r2" << endl;
    cout << "Pop r1" << endl;
    cout << "Div r1, r2" << endl;
    cout << "Push r1" << endl;
}
