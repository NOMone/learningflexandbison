#include "json.h"

#include <iostream>
#include <sstream>
#include <sys/stat.h>

using namespace JSON;
using namespace std;

int main(int, char**) {

    Parser parser;

    // Read the input file,
    const char *inputFileName = "InputFile.txt";
    struct stat fileStat;
    if (stat(inputFileName, &fileStat)) {
        cout << "Couldn't open InputFile.txt" << endl;
        return -1;
    }

    int32_t fileSize = (int32_t) fileStat.st_size;
    char *data = (char *) malloc(fileSize+2); // Account for two additional zeroes.

    FILE *inputFile = fopen(inputFileName, "r");
    fread(data, 1, fileSize, inputFile);
    fclose(inputFile);

    // Terminate the read data with 2 zeroes,
    data[fileSize  ] = 0;
    data[fileSize+1] = 0;

    // Parse!
    Value *value = parser.parseInPlace(data, fileSize+2);

    // Delete the file data,
    free(data);

    // Print out parser errors,
    parser.printErrors();

    // Print out the parsed JSON,
    cout << value->toString();

    // Delete the parsed JSON structure,
    delete value;
}
