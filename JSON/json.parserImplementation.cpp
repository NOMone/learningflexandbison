#include "json.parserImplementation.h"

#include <sstream>

using namespace JSON;

std::string Item::toString() {
    std::stringstream stringStream;
    type(stringStream);
    return stringStream.str();
}

void Null::type(
        std::stringstream &stringStream,
        const std::string &indentationStep, const std::string &indentation) {
    stringStream << "null";
}

void Boolean::type(
        std::stringstream &stringStream,
        const std::string &indentationStep, const std::string &indentation) {
    stringStream << (value ? "true" : "false");
}

void Number::type(
        std::stringstream &stringStream,
        const std::string &indentationStep, const std::string &indentation) {
    stringStream << value;
}

void String::type(
        std::stringstream &stringStream,
        const std::string &indentationStep, const std::string &indentation) {
    stringStream << '"' << value << '"';
}

Array::Array(Item *parent) : Value(parent) {}
Array::~Array() {
    for (int32_t i=(int32_t) values.size()-1; i>-1; i--) delete values[i];
}

void Array::type(
        std::stringstream &stringStream,
        const std::string &indentationStep, const std::string &indentation) {
    int32_t count = (int32_t) values.size();
    int32_t beforeLastIndex = count - 1;
    std::string newIndentation = indentation + indentationStep;
    std::string extraLineFeed1="", extraLineFeed2="";
    if ((!values.empty()) && dynamic_cast<Object *>(values[0])){
        extraLineFeed1 = "\n" + newIndentation;
        extraLineFeed2 = "\n" + indentation;
    }
    stringStream << '[' << extraLineFeed1;
    for (int32_t i=0; i<beforeLastIndex; i++) {
        values[i]->type(stringStream, indentationStep, newIndentation);
        stringStream << ", " << extraLineFeed1;
    }
    if (count) values[beforeLastIndex]->type(stringStream, indentationStep, newIndentation);
    stringStream << extraLineFeed2 << ']';
}

Pair::Pair(Item *parent, const std::string &key) : Item(parent), key(key) {}

Pair::~Pair() {
    delete value;
}

void Pair::type(
        std::stringstream &stringStream,
        const std::string &indentationStep, const std::string &indentation) {
    stringStream << '"' << key << "\": ";
    value->type(stringStream, indentationStep, indentation);
}

Object::Object(Item *parent) : Value(parent) {}

Object::~Object() {
    for (int32_t i=(int32_t) pairs.size()-1; i>-1; i--) delete pairs[i];
}

void Object::type(
        std::stringstream &stringStream,
        const std::string &indentationStep, const std::string &indentation) {
    int32_t count = (int32_t) pairs.size();
    int32_t beforeLastIndex = count - 1;
    std::string newIndentation = indentation + indentationStep;
    stringStream << "{\n";
    for (int32_t i=0; i<beforeLastIndex; i++) {
        stringStream << newIndentation;
        pairs[i]->type(stringStream, indentationStep, newIndentation);
        stringStream << ",\n";
    }
    if (count) {
        stringStream << newIndentation;
        pairs[beforeLastIndex]->type(stringStream, indentationStep, newIndentation);
    }
    stringStream << '\n' << indentation << "}";
}

Pair *Object::get(const std::string &key) {

    for (int32_t i=(int32_t) pairs.size()-1; i>-1; i--) {
        if (pairs[i]->key == key) return pairs[i];
    }
    return nullptr;
}

ParserImplementation::ParserImplementation() {}

Value *ParserImplementation::finishParsing() {

    // Keep the value on top of the stack,
    Value *parsedValue = nullptr;
    if (!itemsStack.empty()) {
        parsedValue = (Value *) itemsStack.top();
        itemsStack.pop();
    }

    // Delete any remaining values (which are only there due to errors in parsing),
    while (!itemsStack.empty()) {
        delete itemsStack.top();
        itemsStack.pop();
    }

    // Reset everything else,
    currentContainer = nullptr;
    stringTokens.clear();
    errors.clear();
    return parsedValue;
}

bool JSON::decode(const std::string &inputText, std::string &outputText) {

    int32_t length = (int32_t) inputText.length();
    int32_t lastCharIndex = length-1;

    outputText.clear();
    outputText.reserve(length);

    for (int32_t i=0; i<length; i++) {

        if (inputText[i] == '\\') {
            if (i == lastCharIndex) return false;

            i++;
            switch(inputText[i]) {
                case '"' : outputText += '"' ; break;
                case '\\': outputText += '\\'; break;
                case '/' : outputText += '/' ; break;
                case 'b' : outputText += '\b'; break;
                case 'f' : outputText += '\f'; break;
                case 'n' : outputText += '\n'; break;
                case 'r' : outputText += '\r'; break;
                case 't' : outputText += '\t'; break;
                case 'u' : {
                    int32_t charCode;
                    if (sscanf(&inputText[i+1], "%04x", &charCode)) {

                        // Sorry, currently we don't have UTF-8 support,
                        outputText += '\254';

                        /*
                        // #include <locale>
                        wchar_t wc = charCode;
                        std::codecvt_utf8<wchar_t> conv;
                        const wchar_t *wnext;
                        char *next;
                        char cbuf[4] = {0}; // initialize the buffer to 0 to have a terminating null
                        std::mbstate_t state;
                        conv.out(state, &wc, &wc + 1, wnext, cbuf, cbuf+4, next);

                        outputText += cbuf;
                        */

                        i += 4;
                        break;
                    } else {
                        return false;
                    }
                }
                default: return false;
            }
        } else {
            outputText += inputText[i];
        }
    }

    return true;
}
