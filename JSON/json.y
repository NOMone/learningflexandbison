%{

#include "json.parserImplementation.h"
#include <stdint.h>

// Set the api prefix (don't forget to change the
// parsing options "%define api.prefix" line,
#define yy(name) JSON_ ## name
#define YY(name) JSON_ ## name

// Required declarations,
union YY(STYPE);
typedef void* yyscan_t;

int  yy(lex)           (YY(STYPE) *yy(lval_param), yyscan_t yyscanner);
void yy(error)         (yyscan_t scanner, JSON::ParserImplementation *parser, const char *s);

%}

%define api.pure full
%define api.prefix {JSON_}

%param { yyscan_t scanner }
%parse-param { JSON::ParserImplementation *parser }

// Bison fundamentally works by asking flex to get the next token, which it
// returns as an object of type "yystype".  But tokens could be of any
// arbitrary data type!  So we deal with that in Bison by defining a C union
// holding each of the types of tokens that Flex could return, and have Bison
// use that union instead of "int" for the definition of "yystype":
%union {
    bool boolean;   
    double number;
    int32_t stringIndex;
}

// define the constant-string tokens:
%token NILL

// define the "terminal symbol" token types I'm going to use (in CAPS
// by convention), and associate each with a field of the union:
%token <boolean> BOOLEAN
%token <number> NUMBER
%token <stringIndex> STRING

%%
value:
    NILL            { parser->pushNull(); }
    | BOOLEAN       { parser->pushBoolean($1); }
    | NUMBER        { parser->pushNumber($1); }
    | STRING        { parser->pushString($1); }
    | object        
    | array         
    ;

objectStartProc:                        { parser->startObject(); }
object:
    '{' '}'                             { parser->pushEmptyObject(); }
    | '{' objectStartProc members '}'   { parser->endObject(); }
    ;
        
members:
   pair               
   | members ',' pair
   ;

pairStartProc: STRING           { parser->startPair($1); };
pair:
    pairStartProc ':' value     { parser->endPair(); };

arrayStartProc:                     { parser->startArray(); }   
array:
    '[' ']'                         { parser->pushEmptyArray(); }
    | '[' arrayStartProc values ']' { parser->endArray(); }
    ;
   
values:
    value
    | values ',' value
    ;
    
%%