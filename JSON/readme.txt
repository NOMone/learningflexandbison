Attempting to parse JSON data into a C++ data-structure.

Example to parse (take from here: https://en.wikipedia.org/wiki/JSON):
======================================================================
{
  "$schema": "http://json-schema.org/schema#",
  "title": "Product",
  "type": "object",
  "required": ["id", "name", "price"],
  "properties": {
    "id": {
      "type": "number",
      "description": "Product identifier"
    },
    "name": {
      "type": "string",
      "description": "Name of the product"
    },
    "price": {
      "type": "number",
      "minimum": 0
    },
    "tags": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "stock": {
      "type": "object",
      "properties": {
        "warehouse": {
          "type": "number"
        },
        "retail": {
          "type": "number"
        }
      }
    }
  }
}

Literals:
=========
String => "bla bla \"bla bla  "
number => 1234 or 123.456
boolean => true or false
null
:
,
{
}
[
]
