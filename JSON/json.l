%{
#include <stdint.h>

#include "json.parserImplementation.h"
#include "json.parser.h"

using namespace JSON;

#define YYSTYPE JSON_STYPE

%}
%option noyywrap
%option reentrant
%option extra-type="JSON::ParserImplementation *"
%option bison-bridge
%option prefix="JSON_"

int         {digit}|[1-9]{digits}|-{digit}|-[1-9]{digits}
digit       [0-9]
digits      {digit}{digit}*
frac        \.{digits}
exp         {e}{digits}
e           e|e\+|e\-|E|E\+|E\-

%%
[ \t] ;
\n                      { yyget_extra(yyscanner)->lineNumber++; }
"null"                  { return NILL; }
"true"|"false"          {
                            if (!strcmp(yytext, "true")) {
                                yylval->boolean = true;
                            } else {
                                yylval->boolean = false;
                            }
                            return BOOLEAN;  
                        }
                        
{int}                   { yylval->number = atoi(yytext); return NUMBER; }
{int}{frac}|{int}{exp}|{int}{frac}{exp} { 
                            yylval->number = atof(yytext); return NUMBER; 
                        }
                                
\"([^\x00-\x1F\\\"]|\\\"|\\\\|\\\/|\\b|\\f|\\n|\\r|\\t|\\u[0-9a-fA-F]{4})*\"  { 
                            // we have to copy because we can't rely on yytext not changing underneath us:
                            yylval->stringIndex = yyget_extra(yyscanner)->addStringTokenStripQoutes(yytext);
                            return STRING;
                        }                        
.                       { return *yytext; }
%%
