 %{
#include <cstdio>
#include <iostream>
using namespace std;

// stuff from flex that bison needs to know about:
extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;
 
void yyerror(const char *s);
%}

// Bison fundamentally works by asking flex to get the next token, which it
// returns as an object of type "yystype".  But tokens could be of any
// arbitrary data type!  So we deal with that in Bison by defining a C union
// holding each of the types of tokens that Flex could return, and have Bison
// use that union instead of "int" for the definition of "yystype":
%union {
	int ival;
	float fval;
	char *sval;
}

// define the constant-string tokens:
//%token SNAZZLE TYPE
//%token END

// define the "terminal symbol" token types I'm going to use (in CAPS
// by convention), and associate each with a field of the union:
%token <ival> INT
%token <fval> FLOAT
%token <sval> STRING

%%
Expression:
	Precedence1 { cout << "Expression!\n"; } // 1 means lowest precedence.
	;

Precedence1:
	Precedence1 '+' Precedence2    { cout << "Add\n"; }
	| Precedence1 '-' Precedence2	 { cout << "Sub\n"; }
	| Precedence2	
	;

Precedence2:
	Precedence2 '*' Precedence3    { cout << "Mul\n"; }
	| Precedence2 '/' Precedence3  { cout << "Div\n"; }
	| Precedence3
	;

Precedence3:
	INT   { cout << "INT: " << $1 << endl; }
	;

%%

int main(int, char**) {
	// open a file handle to a particular file:
	FILE *myfile = fopen("InputFile.txt", "r");
	// make sure it's valid:
	if (!myfile) {
		cout << "Couldn't open InputFile.txt" << endl;
		return -1;
	}
	// set flex to read from it instead of defaulting to STDIN:
	yyin = myfile;

	// parse through the input until there is no more:
	do {
		yyparse();
	} while (!feof(yyin));
	
}

void yyerror(const char *s) {
	cout << "EEK, parse error!  Message: " << s << endl;
	// might as well halt now:
	exit(-1);
}
