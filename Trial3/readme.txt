Parsing operators with different precendences.

Refere to this: http://stackoverflow.com/a/10988746/1942069

- Terminals (like ids/names or immediate values) are of the highest precedence. They are the only thing that has to be evaluated immediately everytime. 
- Lower precedence rules must be functions in higher precedency ones. For example:

	stmt : NAME '=' expr
		  | expr
		  ;

	expr : expr '+' term
		  | expr '-' term
		  | term
		  ;

	term : term '*' factor
		  | term '/' factor
		  | factor
		  ;

	factor : '(' expr ')'
		    | '-' factor
		    | NUMBER
		    ;

Taking this line:
	expr : expr '+' term

To the rhs of the '+' sign, there can only be an item that has to be evaluated first (of higher precedence, like a NUMBER), or otherwise ambiguity is introduced due to the uncertainity of which has to be evaluted first.
